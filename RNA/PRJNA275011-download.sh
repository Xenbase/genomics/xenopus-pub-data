#!/bin/bash

#ClutchB_polyA_11.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795702/SRR1795702_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795702/SRR1795702_2.fastq.gz
#ClutchB_polyA_15_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795709/SRR1795709_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795709/SRR1795709_2.fastq.gz
#ClutchB_polyA_16_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795711/SRR1795711_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795711/SRR1795711_2.fastq.gz
#ClutchB_polyA_20.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795720/SRR1795720_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795720/SRR1795720_2.fastq.gz
#ClutchB_polyA_22.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795724/SRR1795724_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795724/SRR1795724_2.fastq.gz
#ClutchB_polyA_23_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795725/SRR1795725_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795725/SRR1795725_2.fastq.gz
#ClutchB_polyA_13.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795706/SRR1795706_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795706/SRR1795706_2.fastq.gz
#ClutchB_polyA_14_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795707/SRR1795707_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795707/SRR1795707_2.fastq.gz
#ClutchA_polyA_0_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795535/SRR1795535_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795535/SRR1795535_2.fastq.gz
#ClutchA_polyA_1_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795537/SRR1795537_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795537/SRR1795537_2.fastq.gz
#ClutchA_polyA_2_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795539/SRR1795539_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795539/SRR1795539_2.fastq.gz
#ClutchA_polyA_2.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795540/SRR1795540_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795540/SRR1795540_2.fastq.gz
#ClutchA_polyA_3_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795541/SRR1795541_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795541/SRR1795541_2.fastq.gz
#ClutchA_polyA_3.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795542/SRR1795542_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795542/SRR1795542_2.fastq.gz
#ClutchA_polyA_5.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795546/SRR1795546_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795546/SRR1795546_2.fastq.gz
#ClutchA_polyA_7.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795550/SRR1795550_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795550/SRR1795550_2.fastq.gz
#ClutchA_polyA_8_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795551/SRR1795551_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795551/SRR1795551_2.fastq.gz
#ClutchA_polyA_11.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795558/SRR1795558_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795558/SRR1795558_2.fastq.gz
#ClutchA_polyA_12_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795559/SRR1795559_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795559/SRR1795559_2.fastq.gz
#ClutchA_polyA_14_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795563/SRR1795563_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795563/SRR1795563_2.fastq.gz
#ClutchA_polyA_15.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795566/SRR1795566_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795566/SRR1795566_2.fastq.gz
#ClutchA_polyA_16_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795567/SRR1795567_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795567/SRR1795567_2.fastq.gz
#ClutchA_polyA_16.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795568/SRR1795568_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795568/SRR1795568_2.fastq.gz
#ClutchA_polyA_17_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795569/SRR1795569_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795569/SRR1795569_2.fastq.gz
#ClutchA_polyA_17.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795570/SRR1795570_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795570/SRR1795570_2.fastq.gz
#ClutchA_polyA_18.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795572/SRR1795572_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795572/SRR1795572_2.fastq.gz
#ClutchA_polyA_19_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795573/SRR1795573_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795573/SRR1795573_2.fastq.gz
#ClutchA_polyA_21_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795577/SRR1795577_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795577/SRR1795577_2.fastq.gz
#ClutchA_polyA_27_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795586/SRR1795586_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795586/SRR1795586_2.fastq.gz
#ClutchA_polyA_31_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795590/SRR1795590_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795590/SRR1795590_2.fastq.gz
#ClutchA_polyA_32_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795591/SRR1795591_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795591/SRR1795591_2.fastq.gz
#ClutchA_polyA_33_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795592/SRR1795592_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795592/SRR1795592_2.fastq.gz
#ClutchA_polyA_37_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795596/SRR1795596_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795596/SRR1795596_2.fastq.gz
#ClutchA_polyA_55_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795613/SRR1795613_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795613/SRR1795613_2.fastq.gz
#ClutchA_polyA_56_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795614/SRR1795614_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795614/SRR1795614_2.fastq.gz
#ClutchA_polyA_59_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795617/SRR1795617_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795617/SRR1795617_2.fastq.gz
#ClutchA_polyA_61_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795619/SRR1795619_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795619/SRR1795619_2.fastq.gz
#ClutchA_polyA_65_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795623/SRR1795623_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795623/SRR1795623_2.fastq.gz
#ClutchA_polyA_33_hpf_repeat
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795628/SRR1795628_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795628/SRR1795628_2.fastq.gz
#ClutchA_polyA_52_hpf_repeat
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795629/SRR1795629_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795629/SRR1795629_2.fastq.gz
#ClutchA_polyA_64_hpf_repeat
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795630/SRR1795630_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795630/SRR1795630_2.fastq.gz
#ClutchA_ribozero_5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795632/SRR1795632_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795632/SRR1795632_2.fastq.gz
#ClutchA_ribozero_5.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795634/SRR1795634_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795634/SRR1795634_2.fastq.gz
#ClutchA_ribozero_6_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795635/SRR1795635_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795635/SRR1795635_2.fastq.gz
#ClutchA_ribozero_8_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795639/SRR1795639_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795639/SRR1795639_2.fastq.gz
#ClutchA_ribozero_8.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795640/SRR1795640_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795640/SRR1795640_2.fastq.gz
#ClutchA_ribozero_11.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795647/SRR1795647_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795647/SRR1795647_2.fastq.gz
#ClutchA_ribozero_12.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795649/SRR1795649_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795649/SRR1795649_2.fastq.gz
#ClutchA_ribozero_14_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795652/SRR1795652_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795652/SRR1795652_2.fastq.gz
#ClutchA_ribozero_15.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795656/SRR1795656_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795656/SRR1795656_2.fastq.gz
#ClutchA_ribozero_16.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795658/SRR1795658_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795658/SRR1795658_2.fastq.gz
#ClutchA_ribozero_18_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795661/SRR1795661_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795661/SRR1795661_2.fastq.gz
#ClutchA_ribozero_20_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795665/SRR1795665_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795665/SRR1795665_2.fastq.gz
#ClutchA_ribozero_2_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795666/SRR1795666_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795666/SRR1795666_2.fastq.gz
#ClutchA_ribozero_20.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795667/SRR1795667_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795667/SRR1795667_2.fastq.gz
#ClutchA_ribozero_21.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795669/SRR1795669_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795669/SRR1795669_2.fastq.gz
#ClutchA_ribozero_22_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795670/SRR1795670_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795670/SRR1795670_2.fastq.gz
#ClutchA_ribozero_22.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795671/SRR1795671_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795671/SRR1795671_2.fastq.gz
#ClutchA_ribozero_23.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795673/SRR1795673_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795673/SRR1795673_2.fastq.gz
#ClutchA_ribozero_3.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795676/SRR1795676_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795676/SRR1795676_2.fastq.gz
#ClutchB_polyA_0.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795680/SRR1795680_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795680/SRR1795680_2.fastq.gz
#ClutchB_polyA_1_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795681/SRR1795681_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795681/SRR1795681_2.fastq.gz
#ClutchB_polyA_3_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795685/SRR1795685_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795685/SRR1795685_2.fastq.gz
#ClutchB_polyA_4.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795688/SRR1795688_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795688/SRR1795688_2.fastq.gz
#ClutchB_polyA_5.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795690/SRR1795690_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795690/SRR1795690_2.fastq.gz
#ClutchB_polyA_7_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795693/SRR1795693_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795693/SRR1795693_2.fastq.gz
#ClutchB_polyA_8.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795696/SRR1795696_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795696/SRR1795696_2.fastq.gz
#ClutchB_polyA_9_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795697/SRR1795697_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795697/SRR1795697_2.fastq.gz
#ClutchB_polyA_14.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795708/SRR1795708_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795708/SRR1795708_2.fastq.gz
#ClutchB_polyA_21_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795721/SRR1795721_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795721/SRR1795721_2.fastq.gz
#ClutchB_polyA_10_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/002/SRR2972862/SRR2972862_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/002/SRR2972862/SRR2972862_2.fastq.gz
#ClutchB_polyA_17.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/004/SRR2972864/SRR2972864_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/004/SRR2972864/SRR2972864_2.fastq.gz
#ClutchB_polyA_18.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795716/SRR1795716_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795716/SRR1795716_2.fastq.gz
#ClutchB_polyA_23.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795726/SRR1795726_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795726/SRR1795726_2.fastq.gz
#ClutchB_polyA_11_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795701/SRR1795701_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795701/SRR1795701_2.fastq.gz
#ClutchB_polyA_13_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795705/SRR1795705_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795705/SRR1795705_2.fastq.gz
#ClutchB_polyA_15.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795710/SRR1795710_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795710/SRR1795710_2.fastq.gz
#ClutchB_polyA_19.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795718/SRR1795718_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795718/SRR1795718_2.fastq.gz
#ClutchA_polyA_1.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795538/SRR1795538_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795538/SRR1795538_2.fastq.gz
#ClutchA_polyA_5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795545/SRR1795545_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795545/SRR1795545_2.fastq.gz
#ClutchA_polyA_6_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795547/SRR1795547_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795547/SRR1795547_2.fastq.gz
#ClutchA_polyA_6.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795548/SRR1795548_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795548/SRR1795548_2.fastq.gz
#ClutchA_polyA_7_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795549/SRR1795549_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795549/SRR1795549_2.fastq.gz
#ClutchA_polyA_9_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795553/SRR1795553_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795553/SRR1795553_2.fastq.gz
#ClutchA_polyA_9.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795554/SRR1795554_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795554/SRR1795554_2.fastq.gz
#ClutchA_polyA_10.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795556/SRR1795556_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795556/SRR1795556_2.fastq.gz
#ClutchA_polyA_12.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795560/SRR1795560_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795560/SRR1795560_2.fastq.gz
#ClutchA_polyA_13_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795561/SRR1795561_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795561/SRR1795561_2.fastq.gz
#ClutchA_polyA_15_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795565/SRR1795565_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795565/SRR1795565_2.fastq.gz
#ClutchA_polyA_19.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795574/SRR1795574_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795574/SRR1795574_2.fastq.gz
#ClutchA_polyA_20_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795575/SRR1795575_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795575/SRR1795575_2.fastq.gz
#ClutchA_polyA_20.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795576/SRR1795576_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795576/SRR1795576_2.fastq.gz
#ClutchA_polyA_22_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795579/SRR1795579_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795579/SRR1795579_2.fastq.gz
#ClutchA_polyA_22.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795580/SRR1795580_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795580/SRR1795580_2.fastq.gz
#ClutchA_polyA_23_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795581/SRR1795581_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795581/SRR1795581_2.fastq.gz
#ClutchA_polyA_26_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795585/SRR1795585_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795585/SRR1795585_2.fastq.gz
#ClutchA_polyA_29_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795588/SRR1795588_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795588/SRR1795588_2.fastq.gz
#ClutchA_polyA_30_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795589/SRR1795589_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795589/SRR1795589_2.fastq.gz
#ClutchA_polyA_34_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795593/SRR1795593_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795593/SRR1795593_2.fastq.gz
#ClutchA_polyA_39_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795598/SRR1795598_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795598/SRR1795598_2.fastq.gz
#ClutchA_polyA_40_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795599/SRR1795599_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795599/SRR1795599_2.fastq.gz
#ClutchA_polyA_42_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795601/SRR1795601_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795601/SRR1795601_2.fastq.gz
#ClutchA_polyA_49_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795607/SRR1795607_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795607/SRR1795607_2.fastq.gz
#ClutchA_polyA_50_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795608/SRR1795608_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795608/SRR1795608_2.fastq.gz
#ClutchA_polyA_52_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795610/SRR1795610_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795610/SRR1795610_2.fastq.gz
#ClutchA_polyA_54_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795612/SRR1795612_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795612/SRR1795612_2.fastq.gz
#ClutchA_polyA_57_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795615/SRR1795615_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795615/SRR1795615_2.fastq.gz
#ClutchA_polyA_58_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795616/SRR1795616_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795616/SRR1795616_2.fastq.gz
#ClutchA_polyA_60_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795618/SRR1795618_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795618/SRR1795618_2.fastq.gz
#ClutchA_polyA_62_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795620/SRR1795620_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795620/SRR1795620_2.fastq.gz
#ClutchA_polyA_63_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795621/SRR1795621_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795621/SRR1795621_2.fastq.gz
#ClutchA_polyA_64_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795622/SRR1795622_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795622/SRR1795622_2.fastq.gz
#ClutchA_polyA_11.5_hpf_repeat
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795625/SRR1795625_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795625/SRR1795625_2.fastq.gz
#ClutchA_polyA_30_hpf_repeat
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795627/SRR1795627_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795627/SRR1795627_2.fastq.gz
#ClutchA_ribozero_6.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795636/SRR1795636_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795636/SRR1795636_2.fastq.gz
#ClutchA_ribozero_7_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795637/SRR1795637_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795637/SRR1795637_2.fastq.gz
#ClutchA_ribozero_9_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795641/SRR1795641_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795641/SRR1795641_2.fastq.gz
#ClutchA_ribozero_10_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795643/SRR1795643_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795643/SRR1795643_2.fastq.gz
#ClutchA_ribozero_11_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795646/SRR1795646_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795646/SRR1795646_2.fastq.gz
#ClutchA_ribozero_13.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795651/SRR1795651_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795651/SRR1795651_2.fastq.gz
#ClutchA_ribozero_14.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795653/SRR1795653_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795653/SRR1795653_2.fastq.gz
#ClutchA_ribozero_16_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795657/SRR1795657_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795657/SRR1795657_2.fastq.gz
#ClutchA_ribozero_18.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795662/SRR1795662_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795662/SRR1795662_2.fastq.gz
#ClutchA_ribozero_19_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795663/SRR1795663_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795663/SRR1795663_2.fastq.gz
#ClutchA_ribozero_2.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795674/SRR1795674_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795674/SRR1795674_2.fastq.gz
#ClutchA_ribozero_3_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795675/SRR1795675_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795675/SRR1795675_2.fastq.gz
#ClutchA_ribozero_4.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795678/SRR1795678_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795678/SRR1795678_2.fastq.gz
#ClutchB_polyA_3.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795686/SRR1795686_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795686/SRR1795686_2.fastq.gz
#ClutchB_polyA_4_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795687/SRR1795687_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795687/SRR1795687_2.fastq.gz
#ClutchB_polyA_6_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795691/SRR1795691_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795691/SRR1795691_2.fastq.gz
#ClutchB_polyA_8_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795695/SRR1795695_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795695/SRR1795695_2.fastq.gz
#ClutchB_polyA_18_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/005/SRR2972865/SRR2972865_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/005/SRR2972865/SRR2972865_2.fastq.gz
#ClutchB_polyA_12.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795704/SRR1795704_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795704/SRR1795704_2.fastq.gz
#ClutchB_polyA_16.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795712/SRR1795712_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795712/SRR1795712_2.fastq.gz
#ClutchB_polyA_17_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795713/SRR1795713_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795713/SRR1795713_2.fastq.gz
#ClutchB_polyA_19_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795717/SRR1795717_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795717/SRR1795717_2.fastq.gz
#ClutchB_polyA_20_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795719/SRR1795719_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795719/SRR1795719_2.fastq.gz
#ClutchB_polyA_21.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795722/SRR1795722_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795722/SRR1795722_2.fastq.gz
#ClutchB_polyA_24_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795727/SRR1795727_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795727/SRR1795727_2.fastq.gz
#ClutchA_polyA_0.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795536/SRR1795536_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795536/SRR1795536_2.fastq.gz
#ClutchA_polyA_4_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795543/SRR1795543_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795543/SRR1795543_2.fastq.gz
#ClutchA_polyA_4.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795544/SRR1795544_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795544/SRR1795544_2.fastq.gz
#ClutchA_polyA_8.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795552/SRR1795552_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795552/SRR1795552_2.fastq.gz
#ClutchA_polyA_10_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795555/SRR1795555_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795555/SRR1795555_2.fastq.gz
#ClutchA_polyA_11_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795557/SRR1795557_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795557/SRR1795557_2.fastq.gz
#ClutchA_polyA_13.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795562/SRR1795562_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795562/SRR1795562_2.fastq.gz
#ClutchA_polyA_14.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795564/SRR1795564_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795564/SRR1795564_2.fastq.gz
#ClutchA_polyA_18_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795571/SRR1795571_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795571/SRR1795571_2.fastq.gz
#ClutchA_polyA_21.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795578/SRR1795578_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795578/SRR1795578_2.fastq.gz
#ClutchA_polyA_23.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795582/SRR1795582_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795582/SRR1795582_2.fastq.gz
#ClutchA_polyA_24_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795583/SRR1795583_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795583/SRR1795583_2.fastq.gz
#ClutchA_polyA_25_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795584/SRR1795584_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795584/SRR1795584_2.fastq.gz
#ClutchA_polyA_28_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795587/SRR1795587_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795587/SRR1795587_2.fastq.gz
#ClutchA_polyA_35_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795594/SRR1795594_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795594/SRR1795594_2.fastq.gz
#ClutchA_polyA_36_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795595/SRR1795595_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795595/SRR1795595_2.fastq.gz
#ClutchA_polyA_38_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795597/SRR1795597_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795597/SRR1795597_2.fastq.gz
#ClutchA_polyA_41_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795600/SRR1795600_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795600/SRR1795600_2.fastq.gz
#ClutchA_polyA_44_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795602/SRR1795602_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795602/SRR1795602_2.fastq.gz
#ClutchA_polyA_45_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795603/SRR1795603_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795603/SRR1795603_2.fastq.gz
#ClutchA_polyA_46_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795604/SRR1795604_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795604/SRR1795604_2.fastq.gz
#ClutchA_polyA_47_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795605/SRR1795605_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795605/SRR1795605_2.fastq.gz
#ClutchA_polyA_48_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795606/SRR1795606_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795606/SRR1795606_2.fastq.gz
#ClutchA_polyA_51_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795609/SRR1795609_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795609/SRR1795609_2.fastq.gz
#ClutchA_polyA_53_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795611/SRR1795611_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795611/SRR1795611_2.fastq.gz
#ClutchA_polyA_66_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795624/SRR1795624_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795624/SRR1795624_2.fastq.gz
#ClutchA_polyA_21.5_hpf_repeat
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795626/SRR1795626_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/006/SRR1795626/SRR1795626_2.fastq.gz
#ClutchA_ribozero_0_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795631/SRR1795631_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/001/SRR1795631/SRR1795631_2.fastq.gz
#ClutchA_ribozero_0.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795633/SRR1795633_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795633/SRR1795633_2.fastq.gz
#ClutchA_ribozero_7.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795638/SRR1795638_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795638/SRR1795638_2.fastq.gz
#ClutchA_ribozero_9.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795642/SRR1795642_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795642/SRR1795642_2.fastq.gz
#ClutchA_ribozero_1_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795644/SRR1795644_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795644/SRR1795644_2.fastq.gz
#ClutchA_ribozero_10.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795645/SRR1795645_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795645/SRR1795645_2.fastq.gz
#ClutchA_ribozero_12_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795648/SRR1795648_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795648/SRR1795648_2.fastq.gz
#ClutchA_ribozero_13_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795650/SRR1795650_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795650/SRR1795650_2.fastq.gz
#ClutchA_ribozero_15_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795654/SRR1795654_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795654/SRR1795654_2.fastq.gz
#ClutchA_ribozero_1.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795655/SRR1795655_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/005/SRR1795655/SRR1795655_2.fastq.gz
#ClutchA_ribozero_17_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795659/SRR1795659_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795659/SRR1795659_2.fastq.gz
#ClutchA_ribozero_17.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795660/SRR1795660_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/000/SRR1795660/SRR1795660_2.fastq.gz
#ClutchA_ribozero_19.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795664/SRR1795664_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795664/SRR1795664_2.fastq.gz
#ClutchA_ribozero_21_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795668/SRR1795668_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795668/SRR1795668_2.fastq.gz
#ClutchA_ribozero_23_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795672/SRR1795672_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795672/SRR1795672_2.fastq.gz
#ClutchA_ribozero_4_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795677/SRR1795677_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/007/SRR1795677/SRR1795677_2.fastq.gz
#ClutchB_polyA_0_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795679/SRR1795679_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795679/SRR1795679_2.fastq.gz
#ClutchB_polyA_1.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795682/SRR1795682_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795682/SRR1795682_2.fastq.gz
#ClutchB_polyA_2_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795683/SRR1795683_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795683/SRR1795683_2.fastq.gz
#ClutchB_polyA_2.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795684/SRR1795684_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795684/SRR1795684_2.fastq.gz
#ClutchB_polyA_5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795689/SRR1795689_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/009/SRR1795689/SRR1795689_2.fastq.gz
#ClutchB_polyA_6.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795692/SRR1795692_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/002/SRR1795692/SRR1795692_2.fastq.gz
#ClutchB_polyA_7.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795694/SRR1795694_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/004/SRR1795694/SRR1795694_2.fastq.gz
#ClutchB_polyA_9.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795698/SRR1795698_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/008/SRR1795698/SRR1795698_2.fastq.gz
#ClutchB_polyA_12_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795703/SRR1795703_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795703/SRR1795703_2.fastq.gz
#ClutchB_polyA_22_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795723/SRR1795723_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR179/003/SRR1795723/SRR1795723_2.fastq.gz
#ClutchB_polyA_10.5_hpf
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/003/SRR2972863/SRR2972863_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR297/003/SRR2972863/SRR2972863_2.fastq.gz
