#!/bin/bash
DATE=$(date +"%Y-%m-%d")

XENLA_TSV="XENLA_study_list."$DATE".tsv"
XENLA_CSV="XENLA_study_list."$DATE".csv"
XENTR_TSV="XENTR_study_list."$DATE".tsv"
XENTR_CSV="XENTR_study_list."$DATE".csv"

curl -o $XENLA_TSV -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'result=read_study&query=tax_eq(8355)&fields=tax_id%2Cstudy_accession%2Cstudy_alias%2Csubmission_accession%2Clibrary_strategy%2Crun_accession%2Cfastq_ftp%2Cstudy_title&format=tsv' "https://www.ebi.ac.uk/ena/portal/api/search" 
awk -F"\t" '{print $5","$2","$3}' $XENLA_TSV | sort | uniq -c | sort -nr > $XENLA_CSV

curl -o $XENTR_TSV -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'result=read_study&query=tax_eq(8364)&fields=tax_id%2Cstudy_accession%2Cstudy_alias%2Csubmission_accession%2Clibrary_strategy%2Crun_accession%2Cfastq_ftp%2Cstudy_title&format=tsv' "https://www.ebi.ac.uk/ena/portal/api/search"
awk -F"\t" '{print $5","$2","$3}' $XENTR_TSV | sort | uniq -c | sort -nr > $XENTR_CSV
